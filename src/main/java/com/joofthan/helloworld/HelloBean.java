package com.joofthan.helloworld;

import javax.enterprise.context.*;
import javax.inject.*;
import lombok.*;


import java.io.Serializable;

@Named
@SessionScoped
@Getter @Setter
public class HelloBean implements Serializable {

	private String name;

}